//
// Created by Ilya on 3/16/24.
//

#ifndef TEST_FFMPEG_MEDIAFILE_H
#define TEST_FFMPEG_MEDIAFILE_H

#include <memory>
#include <string>

extern "C" {
    #include <libavformat/avformat.h>
    #include <libavcodec/avcodec.h>
}


class CMediaFile {
private:
    AVFormatContext*    formatContext{nullptr};
    std::string         fileName;
public:
    CMediaFile();
    ~CMediaFile();

    static void     printVersion();
    void            printFileInfo();
    void            dumpFileInfo();

    bool            openFile(const std::string& file_name);
    std::string     getDurationAsString(uint64_t _duration);

};


#endif //TEST_FFMPEG_MEDIAFILE_H
