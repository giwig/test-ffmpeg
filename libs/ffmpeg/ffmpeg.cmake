include(ExternalProject)

set(FFMPEG_DIR          ${CMAKE_CURRENT_SOURCE_DIR}/libs/ffmpeg/src )
set(FFMPEG_PAK          ${CMAKE_CURRENT_SOURCE_DIR}/dl              )
set(FFMPEG_BIN          ${CMAKE_CURRENT_BINARY_DIR}/ffmpeg-bin      )
set(FFMPEG_LIB1         ${FFMPEG_BIN}/lib/libavcodec.a          )
set(FFMPEG_LIB2         ${FFMPEG_BIN}/lib/libavformat.a         )
set(FFMPEG_LIB3         ${FFMPEG_BIN}/lib/libavutil.a           )
set(FFMPEG_INCLUDES     ${FFMPEG_BIN}/include                       )

file(MAKE_DIRECTORY ${FFMPEG_INCLUDES})

ExternalProject_Add(libffmpeg
        URL                     https://ffmpeg.org/releases/ffmpeg-6.1.1.tar.xz
        URL_HASH                MD5=341d719415b7f95bb59f5016f2864ac6
        DOWNLOAD_DIR            ${FFMPEG_PAK}
        PREFIX                  ${FFMPEG_BIN}
        SOURCE_DIR              ${FFMPEG_DIR}
        CONFIGURE_COMMAND       ${FFMPEG_DIR}/configure --prefix=${FFMPEG_BIN} --disable-programs --disable-doc --target-os=linux --disable-debug --disable-stripping --disable-swresample --disable-swscale --disable-avfilter --disable-network --disable-avdevice --disable-postproc
        BUILD_COMMAND           make
        INSTALL_COMMAND         make install
        BUILD_IN_SOURCE         0
        BUILD_ALWAYS            OFF
        BUILD_BYPRODUCTS        ${FFMPEG_LIB1}
        BUILD_BYPRODUCTS        ${FFMPEG_LIB2}
        BUILD_BYPRODUCTS        ${FFMPEG_LIB3}
        DOWNLOAD_EXTRACT_TIMESTAMP 0
)

add_library(ffmpeg STATIC IMPORTED GLOBAL)

add_dependencies(ffmpeg libffmpeg)

set_property(TARGET ffmpeg PROPERTY
        IMPORTED_LOCATION
        ${FFMPEG_LIB1}
        ${FFMPEG_LIB2}
        ${FFMPEG_LIB3}
)
#set_target_properties(ffmpeg PROPERTIES INTERFACE_INCLUDE_DIRECTORIES ${FFMPEG_INCLUDES})
#set(FFMPEG_INCLUDES ${FFMPEG_INCLUDES})
set(FFMPEG_LIB1 ${FFMPEG_LIB1})
set(FFMPEG_LIB2 ${FFMPEG_LIB2})
set(FFMPEG_LIB3 ${FFMPEG_LIB3})
