#!/bin/bash


apt-get -y update && apt-get -y upgrade
apt-get -y install build-essential
apt-get -y install yasm nasm  liblzma-dev libx11-dev vdpau-driver-all libvdpau-dev libva-drm2 libbz2-dev libva-x11-2 libva-dev ninja-build
