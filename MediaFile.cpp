//
// Created by Ilya on 3/16/24.
//

#include "MediaFile.h"


/**
 * Constructor
 */
CMediaFile::CMediaFile() {
    formatContext = avformat_alloc_context();
}

/**
 * Destructor
 */
CMediaFile::~CMediaFile() {
    avformat_close_input(&formatContext);
    avformat_free_context(formatContext);
}

/**
 * print out version string of ffmpeg library
 */
void CMediaFile::printVersion() {
//    printf("FFMPEG LICENSE:\t%s\n", avformat_license());
    printf("FFMPEG VERSION:\t%s\n\n", av_version_info());

}

/**
 * open media file.
 *
 * @_fileName path to file
 */
bool CMediaFile::openFile(const std::string& _fileName) {
    fileName = _fileName;
    if(0 == avformat_open_input(&formatContext, fileName.c_str(), nullptr, nullptr))
    {
        if(0 == avformat_find_stream_info(formatContext, nullptr))
            return true;
    }
    return false;
}

/**
 * print out media file info.
 *
 */
void CMediaFile::printFileInfo() {
    if(formatContext)
    {
//        printf("Duration:\t%ld\n", formatContext->duration);
        printf("Duration:\t%s\n", getDurationAsString(formatContext->duration).c_str());
        printf("Streams:\t%d\n", formatContext->nb_streams);
        printf("----------------------------------------------------------------------------------\n");

        for (unsigned int i = 0; i < formatContext->nb_streams; ++i) {
            if(AVMEDIA_TYPE_VIDEO == formatContext->streams[i]->codecpar->codec_type)
            {
                printf("VIDEO:\n");

                printf("\tStream ID:\t%d\n", i);
                printf("\tCodec ID:\t%d\n", formatContext->streams[i]->codecpar->codec_id);
                const AVCodec * avc = avcodec_find_decoder(formatContext->streams[i]->codecpar->codec_id);
                if(nullptr == avc)
                {
                    printf("*** Unknown coodec format\n");
                    break;
                }
                printf("\tCodec:\t\t%s\n", avc->long_name);
                printf("\tBitrate:\t%ld kb/s\n", formatContext->streams[i]->codecpar->bit_rate / 1024);
                float fps = av_q2d(formatContext->streams[i]->r_frame_rate);
                if(fps < 90000.0)
                {
                    printf("\tFPS:\t\t%f\n", av_q2d(formatContext->streams[i]->r_frame_rate));
                }
                printf("\tRes:\t\t%dx%d\n", formatContext->streams[i]->codecpar->width, formatContext->streams[i]->codecpar->height);
            }
            if(AVMEDIA_TYPE_AUDIO == formatContext->streams[i]->codecpar->codec_type) {
                printf("AUDIO:\n");
                printf("\tStream ID:\t%d\n", i);
                printf("\tCodec ID:\t%d\n", formatContext->streams[i]->codecpar->codec_id);
                const AVCodec * avc = avcodec_find_decoder(formatContext->streams[i]->codecpar->codec_id);
                if(nullptr == avc)
                {
                    printf("*** Unknown coodec format\n");
                    break;
                }
                printf("\tCodec:\t\t%s\n", avc->long_name);
                printf("\tChannels:\t%d\n", formatContext->streams[i]->codecpar->channels);
                printf("\tBitrate:\t%ld kb/s\n", formatContext->streams[i]->codecpar->bit_rate / 1000);
            }
        }

    } else {
        printf("Error:");
    }

}

/**
 * built-in dump function for media file.
 *
 */
void CMediaFile::dumpFileInfo() {
    av_dump_format(formatContext,0,fileName.c_str(),false);
}

/**
 * helper function to calculate duration of media file to string.
 *
 * @_duration estimate duration in seconds
 * @return Return's the human readable string of duration
 */
std::string CMediaFile::getDurationAsString(uint64_t _duration)
{
    if (formatContext->duration != AV_NOPTS_VALUE)
    {
        char s_duration[32] = {0};
        int hours, mins, secs, us;
        int64_t duration = formatContext->duration + 5000;
        secs  = duration / AV_TIME_BASE;
        us    = duration % AV_TIME_BASE;
        mins  = secs / 60;
        secs %= 60;
        hours = mins / 60;
        mins %= 60;
        sprintf(s_duration, "%02d:%02d:%02d.%02d", hours, mins, secs, (100 * us) / AV_TIME_BASE);
        return s_duration;
    }
    return "0";
}
