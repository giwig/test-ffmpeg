cmake_minimum_required(VERSION 3.21)
project(test_ffmpeg LANGUAGES CXX)


set(CMAKE_CXX_STANDARD 17)

include(libs/ffmpeg/ffmpeg.cmake)

add_executable(${PROJECT_NAME}
        main.cpp
        MediaFile.cpp
        MediaFile.h
)

target_include_directories( ${PROJECT_NAME}     PUBLIC
        ${FFMPEG_INCLUDES}
)
target_link_libraries(      ${PROJECT_NAME}
        ${FFMPEG_LIB1}
        ${FFMPEG_LIB2}
        ${FFMPEG_LIB3}
        -lva-drm -lva -lva-x11 -lva -lvdpau
        -lX11 -lm
        -llzma -lz -lbz2
)

add_dependencies(${PROJECT_NAME} ffmpeg)

