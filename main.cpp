#include <filesystem>
#include "MediaFile.h"

int main(int argc, char* argv[])
{
    std::string fileName;

    //
    if(argc > 1)
    {
        fileName = argv[1];
        if(!std::filesystem::exists(fileName))
        {
            printf("File `%s` not exists\n\n", std::filesystem::path(fileName).filename().c_str());
            return -2;
        }
    } else {
        printf("Example command:\n\n%s FILEPATH.EXT\n\n", std::filesystem::path(argv[0]).filename().c_str());
        return -1;
    }

    CMediaFile mediaFile{};
    CMediaFile::printVersion();
    if(mediaFile.openFile(fileName))
    {
        mediaFile.printFileInfo();
        return 0;
    }
    printf("ERROR: Can' open file %s", fileName.c_str());
    return -3;
}
