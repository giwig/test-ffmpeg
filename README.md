
# FFMPEG 

---

* преверялось в Ubuntu ( Docker ) с <u>FROM ubuntu:latest</u>

### Комманды для обновления системы (работают в Docker)
```
sudo apt-get -y update && apt-get -y upgrade
sudo apt-get -y install build-essential
sudo apt-get -y install yasm nasm liblzma-dev libx11-dev vdpau-driver-all libvdpau-dev libva-drm2 libbz2-dev libva-x11-2 libva-dev ninja-build
```

<br><br><br>
### Скрипты:
<pre>
    ubuntu_update.sh    -   обновлене и подгрузка библиотек для сборки в <b>ubuntu</b>
        example: <b>sudo ./ubuntu_update.sh</b>

    build.sh            -   сборка проекта и библиотеки <b>ffmpeg</b>
        example: <b>./build.sh</b>

    run.sh              -   запуск файла <b>test_ffmpeg</b> в папке <b>build</b>
        example: <b>./run.sh "/path_to/my_media_file.mp4"</b> 
</pre>

<br><br><br><br>

### Примеры работы:

в некоторых файлах содержаться картинки (metadata) в виде потока ;)
```
> ./test_ffmpeg "Ayaz Erdoğan_Ederin Olsun.mp3"

FFMPEG VERSION:	6.1.1

Duration:	00:04:43.22
Streams:	2
----------------------------------------------------------------------------------
AUDIO:
	Stream ID:	0
	Codec ID:	86017
	Codec:		MP3 (MPEG audio layer 3)
	Channels:	2
	Bitrate:	320 kb/s
VIDEO:
	Stream ID:	1
	Codec ID:	7
	Codec:		MJPEG (Motion JPEG)
	Bitrate:	0 kb/s
	FPS:		90000.000000
	Res:		250x250
```

```
> ./test_ffmpeg "0000/7f5c-48f0-4bca-b8d8-d0a569880cfe.m4a"

FFMPEG VERSION:	6.1.1

Duration:	02:02:20.15
Streams:	1
----------------------------------------------------------------------------------
AUDIO:
	Stream ID:	0
	Codec ID:	86018
	Codec:		AAC (Advanced Audio Coding)
	Channels:	2
	Bitrate:	70 kb/s
```

### Ошибки:
некоторые кодеки не распознаются, так как в стандартной сборке они не включены.
```
> ./test_ffmpeg "[Vocal Deep House Mix] - November 2018 Selections #72.webm"

FFMPEG VERSION:	6.1.1

Duration:	00:55:10.00
Streams:	2
----------------------------------------------------------------------------------
VIDEO:
	Stream ID:	0
	Codec ID:	167
	Codec:		Google VP9
	Bitrate:	0 kb/s
	FPS:		29.970030
	Res:		1280x720
AUDIO:
	Stream ID:	1
	Codec ID:	86076
*** Unknown coodec format
```
